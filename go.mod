module gitlab.com/Ma_124/serve

go 1.15

require (
	github.com/alecthomas/kong v0.2.12
	github.com/fvbommel/sortorder v1.0.2
	github.com/sirupsen/logrus v1.7.0
)
