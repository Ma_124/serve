package main

import (
	"bytes"
	"fmt"
	"github.com/alecthomas/kong"
	"github.com/fvbommel/sortorder"
	log "github.com/sirupsen/logrus"
	"html"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
)

var CLI struct {
	Addr        string `    name:"addr"         short:"a" help:"Address to listen on."         default:":8080"`
	Path        string `    name:"path"         short:"p" help:"Path to listen on."            default:"/"`
	HideFavicon bool   `    name:"hide-favicon" short:"f" help:"Hide requests to /favicon.ico" default:"true"`
	LogLevel    string `    name:"log-level"    short:"l" help:"Log level to output."          default:"info" enum:"trace,debug,info,warn,error,fatal,panic"`
	Location    string `arg name:"location"               help:"Paths to serve."               default:"."    type:"path"`
}

func main() {
	kong.Parse(&CLI)

	if ok, _ := regexp.MatchString(`^[0-9]+$`, CLI.Addr); ok {
		CLI.Addr = ":" + CLI.Addr
	}

	log.SetFormatter(&log.TextFormatter{
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		FullTimestamp:             true,
		TimestampFormat:           "15:04:05",
		DisableSorting:            false,
		QuoteEmptyFields:          true,
	})
	lvl, err := log.ParseLevel(CLI.LogLevel)
	if err != nil {
		panic(err)
	}
	log.SetLevel(lvl)
	log.SetOutput(os.Stdout)

	CLI.Path = strings.Trim(filepath.Clean(CLI.Path), "/")
	CLI.Location, err = filepath.Abs(CLI.Location)
	if err != nil {
		log.WithField("err", err).Fatal("could not obtain absolute path of location")
	}

	log.WithField("loc", CLI.Location).Debug("location")

	localAddr := CLI.Addr
	if strings.HasPrefix(localAddr, ":") {
		localAddr = "localhost" + localAddr
	}
	localAddr = "http://" + localAddr
	log.Info("a development server is listening on " + localAddr)
	err = http.ListenAndServe(CLI.Addr, http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if CLI.HideFavicon && strings.TrimPrefix(req.URL.Path, "/") == "favicon.ico" {
			resp.WriteHeader(http.StatusNotFound)
			_, _ = resp.Write([]byte("favicon.ico was disabled"))
			return
		}
		if req.Method != http.MethodGet {
			writeError(resp, req, http.StatusMethodNotAllowed, "method not allowed", nil)
			return
		}
		ppath := filepath.Clean(strings.Trim(req.URL.Path, "/"))
		if !strings.HasPrefix(ppath, CLI.Path) {
			log.WithFields(map[string]interface{}{
				"url":   req.URL,
				"ppath": ppath,
			}).Debug("resolved path")
			if ppath == "." {
				resp.Header().Set("Location", "/"+CLI.Path)
				writeError(resp, req, http.StatusTemporaryRedirect, "redirecting to base path", nil)
				return
			}
			writeError(resp, req, http.StatusNotFound, "missing base path", nil)
			return
		}

		ipath := filepath.Clean(filepath.Join(CLI.Location, strings.TrimPrefix(ppath, CLI.Path)))
		if !strings.HasPrefix(ipath, CLI.Location) {
			writeError(resp, req, http.StatusNotFound, "not found", nil)
			return
		}

		log.WithFields(map[string]interface{}{
			"url":   req.URL,
			"ppath": ppath,
			"ipath": ipath,
		}).Debug("resolved path")

		f, err := os.Open(ipath)
		if err != nil {
			if os.IsNotExist(err) {
				writeError(resp, req, http.StatusNotFound, "not found", nil)
				return
			}
			writeError(resp, req, http.StatusInternalServerError, "open failed", err)
			return
		}
		defer func() {
			err = f.Close()
			if err != nil {
				log.WithFields(map[string]interface{}{
					"file": ipath,
					"err":  err,
				}).Error("close failed")
			}
		}()

		fi, err := f.Stat()
		if err != nil {
			writeError(resp, req, http.StatusInternalServerError, "stat failed", err)
			return
		}

		if fi.IsDir() {
			entries, err := f.Readdir(-1)
			sort.Slice(entries, func(i, j int) bool {
				return sortorder.NaturalLess(entries[i].Name(), entries[j].Name())
			})
			if err != nil {
				writeError(resp, req, http.StatusInternalServerError, "readdir failed", err)
				return
			}
			var buf bytes.Buffer
			buf.WriteString("<table><thead><tr><th>Mode</th><th>Size</th><th>Modification Time</th><th>Name</th></tr></thead><tbody>")
			writeEntry(&buf, ppath, ".", fi)
			for _, entry := range entries {
				writeEntry(&buf, ppath, entry.Name(), entry)
			}
			buf.WriteString("</tbody></table>")
			_, err = io.Copy(resp, &buf)
			if err != nil {
				log.WithFields(map[string]interface{}{
					"url":    req.URL,
					"status": http.StatusOK,
				}).Error("write failed")
			}
			log.WithFields(map[string]interface{}{
				"url":    req.URL,
				"status": http.StatusOK,
			}).Info("served dir")
			return
		}

		http.ServeContent(resp, req, ipath, fi.ModTime(), f)
		log.WithFields(map[string]interface{}{
			"url":    req.URL,
			"status": http.StatusOK,
		}).Info("served file")
	}))
	if err != nil {
		log.WithFields(map[string]interface{}{
			"addr": CLI.Addr,
			"err":  err,
		}).Fatal("HTTP server failed")
	}
	log.Info("bye")
}

func writeEntry(buf *bytes.Buffer, ppath string, name string, fi os.FileInfo) {
	buf.WriteString("<tr><td>")
	buf.WriteString(fi.Mode().String())
	buf.WriteString("</td><td>")
	buf.WriteString(formatBytes(fi.Size()))
	buf.WriteString("</td><td>")
	buf.WriteString(fi.ModTime().Format("02.01. 15:04"))
	buf.WriteString(`</td><td><a href="/`)
	buf.WriteString(url.PathEscape(strings.TrimRight(ppath, "/")))
	buf.WriteString("/")
	buf.WriteString(url.PathEscape(filepath.Base(name)))
	buf.WriteString(`">`)
	buf.WriteString(html.EscapeString(name))
	buf.WriteString("</a></td><tr>")
}

func writeError(resp http.ResponseWriter, req *http.Request, status int, msg string, error error) {
	e := log.WithFields(map[string]interface{}{
		"url":    req.URL,
		"status": status,
	})
	if error != nil {
		e.WithField("err", error).Error(msg)
	} else {
		e.Warn(msg)
	}
	resp.WriteHeader(status)
	_, err := resp.Write([]byte(msg))
	if err != nil {
		log.WithFields(map[string]interface{}{
			"url":    req.URL,
			"status": status,
			"err":    err,
		}).Error("writing body failed")
	}
}

func formatBytes(b int64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cB",
		float64(b)/float64(div), "kMGTPE"[exp])
}
